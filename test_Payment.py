# -*- coding: iso-8859-15 -*-
"""payment FunkLoad test

$Id: $
"""
import unittest
from funkload.FunkLoadTestCase import FunkLoadTestCase
from webunit.utility import Upload
from funkload.utils import Data
#from funkload.utils import xmlrpc_get_credential

class Payment(FunkLoadTestCase):
    """XXX

    This test use a configuration file Payment.conf.
    """

    def setUp(self):
        """Setting up test."""
        self.logd("setUp")
        self.server_url = self.conf_get('main', 'url')
        # XXX here you can setup the credential access like this
        # credential_host = self.conf_get('credential', 'host')
        # credential_port = self.conf_getInt('credential', 'port')
        # self.login, self.password = xmlrpc_get_credential(credential_host,
        #                                                   credential_port,
        # XXX replace with a valid group
        #                                                   'members')

    def test_payment(self):
        # The description should be set in the configuration file
        server_url = self.server_url
        # begin of test ---------------------------------------------

        # /tmp/tmpuBiAH__funkload/watch0014.request
        self.post(server_url + "/payment/cardinfo", params=[
            ['ACTION', 'PAYMENT'],
            ['ENC_TYPE', '1'],
            ['ENC', '67FEC850F852DD3EB7102A0FFB92EC1856749E359641DAD2314EB7056C333B081901D72128E6F2CADB5DDF7359E04EF9'],
            ['ENC_TIME', '7B3BFA32178C08C37AD7F8783798A1BC'],
            ['ENC_CC', ''],
            ['CHECKSUM', '8f6c1855b25a36240a3265b105b2bdb4'],
            ['CHECKSUM_CHARSET', 'ISO-8859-1'],
            ['CONFIRMATION_URL', 'https://wav.bibi.amadeus.com/plnext/garuda-indonesia/BookTripPlan.aICKET=&STATUS=OK&ACTION=BOOK&SITE=CBEECBEE&LANGUAGE=GB&OFFICE_ID=JKTGA08AO'],
            ['CANCELLATION_URL', 'https://wav.bibi.amadeus.com/plnext/garuda-indonesia/BookTripPlan.aICKET=&STATUS=KO&ACTION=BOOK&SITE=CBEECBEE&LANGUAGE=GB&OFFICE_ID=JKTGA08AO'],
            ['KEEPALIVE_URL', 'https://wav.bibi.amadeus.com/plnext/garuda-indonesia/KeepAliveSessiirLQFSFy!250688312!-31804264?SITE=CBEECBEE&LANGUAGE=GB&OFFICE_ID=JKTGA08AO'],
            ['KEEPALIVE_FREQUENCY', '560000'],
            ['PROCESS_AUTH', ''],
            ['PAYMENT_TYPE', 'EXT'],
            ['PAYMENT_REFERENCE', ''],
            ['AMOUNT', '839000'],
            ['CURRENCY', 'IDR'],
            ['VIRTUAL_CC', ''],
            ['RL', '8FUEZ'],
            ['BANKID', ''],
            ['MERCHANT_ID', 'GarudaIndo01'],
            ['COUNTRY_OF_DEPARTURE', 'ID'],
            ['SESSION_ID', 'zzMsrqDyj2vWNzdei0PotqBsIG9AHwCfz16wcujIaVzxirLQFSFy!250688312!-31804264!1443858653441'],
            ['SITE', 'CBEECBEE'],
            ['LANGUAGE', 'GB'],
            ['USER_TITLE', 'MR'],
            ['USER_LAST_NAME', 'Pavel'],
            ['USER_FIRST_NAME', 'Pavel'],
            ['USER_MOBILE_PHONE', '081252351123'],
            ['USER_HOME_PHONE', '081234123412'],
            ['USER_BUSINESS_PHONE', ''],
            ['USER_EMAIL', 'aaaaa@gmail.com'],
            ['ON_HOLD_ELIGIBLE', 'true'],
            ['ON_HOLD_MAX_DATE', 'Tue Oct 06 07:00:00 GMT 2015'],
            ['DEFERRED_PAYMENT_ELIGIBLE', 'true'],
            ['AMOUNT_AIR', '839000'],
            ['AIR#1_AMOUNT#1', '839000'],
            ['AIR#1_AMOUNTWITHSERVICEFEES#1', '839000'],
            ['AIR#1_AMOUNT_WITHOUT_TAX#1', '690000'],
            ['AIR#1_CURRENCYCODE#1', 'IDR'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_AIRLINECODE', 'GA'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_AIRLINENAME', 'Garuda Indonesia'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_BDATE', 'Friday, October 23, 2015 1:20:00 PM'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_BLOCATIONCITYNAME', 'Denpasar-Bali'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_BLOCATIONCODE', 'DPS'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_BLOCATIONCOUNTRYNAME', 'Indonesia'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_BLOCATIONNAME', 'Ngurah Rai'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_BLOCATIONSTATENAME', ''],
            ['AIR#1_ITINERARY#1_SEGMENT#1_BTERMINAL', 'D'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_CABIN#1', 'R'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_EDATE', 'Friday, October 23, 2015 2:05:00 PM'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_ELOCATIONCITYNAME', 'Bandung'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_ELOCATIONCODE', 'BDO'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_ELOCATIONCOUNTRYNAME', 'Indonesia'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_ELOCATIONNAME', 'Husein Sastranegara'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_ELOCATIONSTATENAME', ''],
            ['AIR#1_ITINERARY#1_SEGMENT#1_EQUIPMENTCODE', '738'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_ETERMINAL', ''],
            ['AIR#1_ITINERARY#1_SEGMENT#1_FLIGHTNUMBER', '335'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_FLIGHTTIME', '01h45min'],
            ['AIR#1_ITINERARY#1_SEGMENT#1_OTHERAIRLINECODE', ''],
            ['AIR#1_ITINERARY#1_SEGMENT#1_OTHERAIRLINENAME', ''],
            ['AIR#1_PNR#1_RBD#1', 'H'],
            ['AIR#1_PNR#1_RBD#1_SEGMENTID', '1'],
            ['AIR#1_PNR#1_RBD#1_STATUSNAME', 'Confirmed'],
            ['CANCEL', '0'],
            ['DELIVERYTYPE', 'ETCKT'],
            ['EMAIL', 'aaaaa@gmail.com'],
            ['EXTERNAL_ID', 'BOOKING'],
            ['EXTERNAL_ID#2', 'APEX7'],
            ['EXTERNAL_ID#3', 'APEX7'],
            ['EXTERNAL_ID#4', ''],
            ['EXTERNAL_ID#5', '5501DF36F457AE122767'],
            ['EXTERNAL_ID#6', 'id-en#WEBSITE'],
            ['FARE#1_PRIMARYRECLOC', '32DRG5'],
            ['FARE#1_RECLOC', '32DRG5'],
            ['FARE#1_TRAVELLERTYPE#1_CODE', 'ADT'],
            ['FARE#1_TRAVELLERTYPE#1_NAME', 'Adult'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_AMOUNT', '839000.0'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_AMOUNTWITHOUTTAX', '690000.0'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_CURRENCYCODE', 'IDR'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_CURRENCYNAME', 'Indonesian Rupiah'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_MILESCOST', '0'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_SERVICEFEEAMOUNT', ''],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_TAX#1_AMOUNT', '5000.0'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_TAX#1_CODE', 'YRVB'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_TAX#1_STATUS', 'S'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_TAX#2_AMOUNT', '75000.0'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_TAX#2_CODE', 'D5CB'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_TAX#2_STATUS', 'S'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_TAX#3_AMOUNT', '69000.0'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_TAX#3_CODE', 'IDLO'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_TAX#3_STATUS', 'S'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_TOTALAMOUNT', '839000.0'],
            ['FARE#1_TRAVELLERTYPE#1_PRICE#1_TOTALTAX', '149000.0'],
            ['FARE#1_TRAVELLERTYPE#1_SEGMENT#1_FAREBASIS', 'HOX'],
            ['FARE#1_TRAVELLERTYPE#1_SEGMENT#1_FARECLASS', 'HOX'],
            ['FARE#1_TRAVELLERTYPE#1_SEGMENT#1_ID', '1'],
            ['FARE#1_TRAVELLERTYPE#1_TRAVELLER#1_ID', '1'],
            ['PHONEB', ''],
            ['PHONEH', '081234123412'],
            ['PNR#1_FIRSTNAME#1', 'Pavel'],
            ['PNR#1_LASTNAME#1', 'Pavel'],
            ['PNR#1_TITLECODE#1', 'MR'],
            ['PNR#1_TITLENAME#1', 'Mr'],
            ['PNR#1_TRAVID#1', '1'],
            ['RECORD_LOCATOR#1', '32DRG5'],
            ['TRAVELER_ID', 'Pavel1451']],
            description="Post /payment/cardinfo")

        # end of test -----------------------------------------------

    def tearDown(self):
        """Setting up test."""
        self.logd("tearDown.\n")



if __name__ in ('main', '__main__'):
    unittest.main()
